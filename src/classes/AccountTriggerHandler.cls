public with sharing class AccountTriggerHandler {

    // Instance of class
    private static AccountTriggerHandler handler = null;

    // Boolean to control recursion
    public boolean isExecuting = false;

    // Class constructor
    public AccountTriggerHandler () {
        
    }

    // Create new instance / return existing instance
    public static AccountTriggerHandler getInstance() {

        if (handler == null) {

            handler = new AccountTriggerHandler();
        } 

        return handler;
    }

    // Before Bulk - build collections etc. prior to running other Before logic.
    public void onBeforeBulk (List<Account> newList) {

    	// Only perform logic if this is the first time the trigger has been called in this context
        if (!isExecuting) {

            // Set isExecuting to true to prevent recursion
            isExecuting = true;

            // Perform trigger logic here

            // Now set isExecuting back to false as all logic has been performed
            isExecuting = false;        
        }
    }

    // Before Insert
    public void onBeforeInsert (List<Account> newList) {

        // Only perform logic if this is the first time the trigger has been called in this context
        if (!isExecuting) {

            // Set isExecuting to true to prevent recursion
            isExecuting = true;

            // Perform trigger logic here

            // Now set isExecuting back to false as all logic has been performed
            isExecuting = false;        
        }
    }

    // Before Update
    public void onBeforeUpdate (List<Account> oldList, List<Account> newList, Map<Id, Account> oldMap, Map<Id, Account> newMap) {

        // Only perform logic if this is the first time the trigger has been called in this context
        if (!isExecuting) {

            // Set isExecuting to true to prevent recursion
            isExecuting = true;

            // Perform trigger logic here

            // Now set isExecuting back to false as all logic has been performed
            isExecuting = false;        
        }        
    }

    // Before Delete
    public void onBeforeDelete (List<Account> oldList, Map<Id, Account> oldMap) {

        // Only perform logic if this is the first time the trigger has been called in this context
        if (!isExecuting) {

            // Set isExecuting to true to prevent recursion
            isExecuting = true;

            // Perform trigger logic here

            // Now set isExecuting back to false as all logic has been performed
            isExecuting = false;        
        }
    }

    // After Bulk - build collections etc. prior to running other After logic.
    public void onAfterBulk (List<Account> newList) {

    	// Only perform logic if this is the first time the trigger has been called in this context
        if (!isExecuting) {

            // Set isExecuting to true to prevent recursion
            isExecuting = true;

            // Perform trigger logic here

            // Now set isExecuting back to false as all logic has been performed
            isExecuting = false;        
        }
    }

    // After Insert
    public void onAfterInsert (List<Account> newList, Map<Id, Account> newMap) {

        // Only perform logic if this is the first time the trigger has been called in this context
        if (!isExecuting) {

            // Set isExecuting to true to prevent recursion
            isExecuting = true;

            // Perform trigger logic here

            // Now set isExecuting back to false as all logic has been performed
            isExecuting = false;        
        }
    }

    // After Update
    public void onAfterUpdate (List<Account> oldList, List<Account> newList, Map<Id, Account> oldMap, Map<Id, Account> newMap) {

        // Only perform logic if this is the first time the trigger has been called in this context
        if (!isExecuting) {

            // Set isExecuting to true to prevent recursion
            isExecuting = true;

            // Perform trigger logic here

            // Now set isExecuting back to false as all logic has been performed
            isExecuting = false;        
        }
    }

    // After Delete
    public void onAfterDelete (List<Account> oldList, Map<Id, Account> oldMap) {

        // Only perform logic if this is the first time the trigger has been called in this context
        if (!isExecuting) {

            // Set isExecuting to true to prevent recursion
            isExecuting = true;

            // Perform trigger logic here

            // Now set isExecuting back to false as all logic has been performed
            isExecuting = false;        
        }
    }

    // After Undelete
    public void onAfterUndelete (List<Account> newList) {

        // Only perform logic if this is the first time the trigger has been called in this context
        if (!isExecuting) {

            // Set isExecuting to true to prevent recursion
            isExecuting = true;

            // Perform trigger logic here

            // Now set isExecuting back to false as all logic has been performed
            isExecuting = false;        
        }
    }
}